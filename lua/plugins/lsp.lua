local custom_attach = function(_, bufnr)
  local opts = { buffer = bufnr, remap = false }
  local keymap = vim.keymap.set

  keymap("n", "gd", function()
    vim.lsp.buf.definition()
  end, opts)
  keymap("n", "K", function()
    vim.lsp.buf.hover()
  end, opts)
  keymap("n", "ca", function()
    vim.lsp.buf.code_action()
  end, opts)
  keymap("n", "gl", function()
    vim.diagnostic.open_float()
  end, opts)
  keymap("n", "[d", function()
    vim.diagnostic.goto_next()
  end, opts)
  keymap("n", "]d", function()
    vim.diagnostic.goto_prev()
  end, opts)
  keymap("n", "gr", function()
    vim.lsp.buf.references()
  end, opts)
  keymap("i", "<C-h>", function()
    vim.lsp.buf.signature_help()
  end, opts)
end

return {
  -- lsp servers
  {
    "neovim/nvim-lspconfig",
    opts = {
      inlay_hints = { enabled = true },
      setup = { on_attach = custom_attach },
    },
  },
}
