-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

local keymaps = vim.keymap
local opts = { noremap = true, silent = true }

local discipline = require("user.discipline")

discipline.cowboy()

keymaps.set("n", "<leader>ex", vim.cmd.Ex, { desc = "[ex]plorer open netrw vim file explorer" })

-- Just ignore Q
keymaps.set("n", "Q", "<nop>")
--delete to void register and paste the latest from register
keymaps.set("n", "<leader>p", '"_dP', { desc = "delete to void register and paste the latest from register" })

-- format code
keymaps.set("n", "<leader>jj", "<cmd>lua vim.lsp.buf.format({ async = true })<cr>", { desc = "Format" })

keymaps.set("n", "[c", function()
    require("treesitter-context").go_to_context()
end, { silent = true, desc = "jumping to context" })

-- repalce hex with hls
keymaps.set("n", "<leader>r", function()
    require("user.utils").replaceHexWithHSL()
end)
