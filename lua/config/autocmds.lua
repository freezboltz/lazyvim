-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here
--
-- Autocommands
-- clean trailing whitespace and new line from <EOF>.
local clean = vim.api.nvim_create_augroup("Cleaner", { clear = true })
vim.api.nvim_create_autocmd(
  "BufWritePre",
  { pattern = "*", command = [[%s/\s\+$//e]], group = clean, desc = "Remove trailing white space" }
)
vim.api.nvim_create_autocmd(
  "BufWritePre",
  { pattern = "*", command = [[%s/\n\+\%$//e]], group = clean, desc = "Remove trailing new line" }
)

-- Disable autoformat for all files
vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "*" },
  callback = function()
    vim.b.autoformat = false
  end,
})

-- fix conceallevel for json files
vim.api.nvim_create_autocmd("Filetype", {
  pattern = { "json", "jsonc" },
  callback = function()
    vim.wo.spell = false
    vim.wo.conceallevel = 0
  end,
})
